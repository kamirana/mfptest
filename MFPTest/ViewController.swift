//
//  ViewController.swift
//  MFPTest
//
//  Created by Muhammad Kamran on 3/2/17.
//  Copyright © 2017 Solutions 4 Mobility. All rights reserved.
//

import UIKit
import IBMMobileFirstPlatformFoundation

class ViewController: UIViewController {

    @IBOutlet weak var statusLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    deinit {
        print("Send Request ViewController Deinitialzed!!!")
    }
    
    @IBAction func sendRequestAction(_ sender: Any) {
        statusLabel.text = ""
        
        let url = URL(string: "https://www.google.com")
        let request = WLResourceRequest(url: url, method: WLHttpMethodGet)
        
        request?.send(completionHandler: {[weak self] (response, error) in
            if let error = error {
                self?.statusLabel.text = error.localizedDescription
                return
            }
            self?.statusLabel.text = "Request Successfull! Check Memory Graph Now."
        })
    }
}

